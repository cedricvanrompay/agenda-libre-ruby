require 'test_helper'

# Test some event helper methods
class EventsHelperTest < ActionView::TestCase
  test 'Markdown with empty content' do
    assert_equal '', to_markdown('')
    assert_equal '', to_markdown('  ')
    assert_equal "'", to_markdown("\'")
    assert_equal '', to_markdown('
')
  end

  test 'HTML elements with a space inside' do
    assert_equal '**hello world**', to_markdown('<strong>hello world </strong>')
    assert_equal '**h**,', to_markdown('<strong> h</strong>,')
    assert_equal '**h**,', to_markdown('<strong>h </strong>,')
    assert_equal '*ho***h**,', to_markdown('<em>ho</em><strong>h </strong>,')
    assert_equal '*ho* **h**,', to_markdown('<em>ho</em><strong> h</strong>,')
    assert_equal '*ho* **h**,', to_markdown('<i>ho</i><b> h</b>,')
  end

  test 'HTML titles too strong' do
    assert_equal '**Big**

', to_markdown('<h1>Big</h1>')

    assert_equal '**Big again**

', to_markdown('<h2>Big again</h2>')

    assert_equal '**Too**

', to_markdown('<h3><strong>Too</strong></h3>')
  end
end
