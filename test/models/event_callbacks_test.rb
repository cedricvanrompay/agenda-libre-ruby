require 'test_helper'

# Test event callbacks
class EventCallbacksTest < ActiveSupport::TestCase
  setup do
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
  end

  test 'schedule' do
    event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now, end_time: Time.zone.now + 1.hour,
      description: 'et hop!',
      city: City.first, region: Region.first,
      url: 'http://example.com',
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )
    assert_difference 'Event.count' do
      assert event.save, event.errors.messages
    end
  end

  test 'moderation' do
    event = Event.new(
      title: 'hello world',
      start_time: Time.zone.now + 1.hour, end_time: Time.zone.now + 2.hours,
      repeat: 1, rule: 'monthly',
      description: 'et hop!',
      city: City.first, region: Region.first,
      url: 'http://example.com',
      submitter: 'contact@example.com',
      tag_list: 'hello world'
    )

    assert event.save, event.errors.messages
    assert_not event.moderated?

    assert_difference 'Event.count' do
      event.update moderated: 1
    end

    assert event.moderated?, event.errors.messages
  end
end
