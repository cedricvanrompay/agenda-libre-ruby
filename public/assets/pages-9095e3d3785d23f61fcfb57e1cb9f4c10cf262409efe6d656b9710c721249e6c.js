(function() {
  var showPosition;

  $(document).on('turbolinks:load', function() {
    if (!navigator.geolocation) {
      return;
    }
    $('a.near-me').click(function(event) {
      event.preventDefault();
      window.goto = event.target.href;
      return navigator.geolocation.getCurrentPosition(showPosition, function(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
            return $('ul.regions li#near-me').remove();
        }
      });
    });
    return $('body.pages.show form').submit(function() {
      if ($('#near_location').val() && $('#near_distance').val()) {
        return $('#city').val('');
      } else {
        $('#city').val($('#near_location').val());
        return $('#near_location').val('');
      }
    });
  });

  showPosition = function(position) {
    return location.replace(window.goto.replace('[me]', "[" + (position.coords.latitude.toFixed(2)) + ", " + (position.coords.longitude.toFixed(2)) + "]"));
  };

}).call(this);
