(function() {
  var showPosition;

  $(document).on('turbolinks:load', function() {
    if (!navigator.geolocation) {
      return;
    }
    return $('a.near-me').click(function(event) {
      event.preventDefault();
      window.goto = event.target.href;
      return navigator.geolocation.getCurrentPosition(showPosition, function(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
            return $('ul.regions li#near-me').remove();
        }
      });
    });
  });

  showPosition = function(position) {
    return location.replace(window.goto.replace('[me]', "[" + position.coords.latitude + ", " + position.coords.longitude + "]"));
  };

}).call(this);
