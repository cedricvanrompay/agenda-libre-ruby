(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'input.description',
      content_css: '/assets/application-141b58a2a2e162f2f4b34e1534a6d30ecaafddd03b96b75b8575f59b5a579682.css',
      relative_urls: false,
      entity_encoding: 'raw',
      document_base_url: '/',
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      plugins: 'lists advlist autolink link image charmap paste print preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
