(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'input.description',
      content_css: '/assets/application-8002eadc07c8d58b56bad6f45a5e2607344c903abacfe94121bd35aa25f764a8.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      plugins: 'lists advlist autolink link image charmap paste print preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
