(function() {
  $(document).on('turbolinks:load', function() {
    return $('.pagination .next a').attr('data-remote', true).each(function() {
      return $(document).scroll((function(_this) {
        return function() {
          if ($(_this).visible(true, true)) {
            return $(_this).click().parents('.pagination').hide();
          }
        };
      })(this));
    });
  });

  $(document).on('ajax:success', '.pagination .next a', function(event, data) {
    var next;
    $(this).parents('tfoot').prev().append($('tbody tr', data));
    next = $('.pagination .next a', data).attr('href');
    if (next != null) {
      return $(this).attr('href', next).parents('.pagination').show();
    }
  });

}).call(this);
