(function() {
  var visit;

  $(document).on('turbolinks:load', function() {
    $('table.list td.view a').each(function() {
      return visit($(this));
    });
    return $('.pagination .next a').attr('data-remote', true).each(function() {
      return $(document).scroll((function(_this) {
        return function() {
          if ($(_this).visible(true, true)) {
            return $(_this).click().parents('.pagination').hide();
          }
        };
      })(this));
    });
  });

  $(document).on('ajax:success', '.pagination .next a', function(event, data) {
    var next;
    $(this).parents('tfoot').prev().append($('tbody tr', data)).find('td.view a').each(function() {
      return visit($(this));
    });
    next = $('.pagination .next a', data).attr('href');
    if (next != null) {
      return $(this).attr('href', next).parents('.pagination').show();
    }
  });

  visit = (function(_this) {
    return function(elt) {
      return elt.closest('tr').addClass('view').click(function(event) {
        var target;
        target = $(event.target);
        if (!((target.attr('target') != null) || (target.parents('a').attr('target') != null))) {
          return Turbolinks.visit(elt.attr('href'));
        }
      });
    };
  })(this);

}).call(this);
