(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      width: '100%',
      height: '40em',
      menubar: false,
      branding: false,
      language: 'fr_FR',
      selector: 'input.description',
      content_css: '/assets/application-1026d28c200fb525d92a5552bc812bd5f0277f5a854b5d3cf9ac3d6fe66bc465.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      style_formats_autohide: true,
      toolbar: [' cut copy paste | undo redo | link image media charmap table | code visualblocks searchreplace', ' removeformat bold italic strikethrough superscript subscript | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify alignnone'],
      plugins: 'lists advlist autolink link image charmap paste print preview table fullscreen searchreplace media insertdatetime visualblocks wordcount code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
