# Gives the possibility to organise organisations! :)
class Kind < ApplicationRecord
  has_many :orgas, dependent: :destroy
end
