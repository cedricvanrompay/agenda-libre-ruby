# Manages data related to events' moderation
class Note < ApplicationRecord
  belongs_to :event
  belongs_to :author, class_name: 'User', inverse_of: false

  validates :contents, presence: true

  # Setup the magic time stamp so it uses the "date" column
  def self.timestamp_attributes_for_create
    super << 'date'
  end
end
