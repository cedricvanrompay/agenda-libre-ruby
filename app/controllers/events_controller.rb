# Event life cycle
# This is a central part to this project
class EventsController < ApplicationController
  has_scope :city, :region, :locality, :tag, :daylimit, :year
  has_scope :near, type: :hash, using: %i[location distance]
  has_scope :future, type: :boolean, default: true, only: [:index], if: :future?

  before_action :set_events, only: :index
  before_action :new_event, unless: -> { params[:id].present? }
  before_action :set_event, if: -> { params[:id].present? }
  before_action :check_secret, only: %i[edit preview update destroy]
  rescue_from ActiveRecord::StaleObjectError, with: :locked

  def index
    respond_to do |format|
      format.html
      format.ics
      format.json
      format.rss
      format.xml
    end
  end

  # GET /events/new
  def new; end

  # POST /events/preview
  def preview_create
    @event.valid?
    render action: :new
  end

  # POST /events
  # POST /events.json
  def create
    respond_to do |format|
      if @event.save
        format.html { redirect_to :root, notice: t('.ok') }
        format.json { render action: :show, status: :created, location: @event }
      else
        format.html { render action: :new }
        status = :unprocessable_entity
        format.json { render json: @event.errors, status: status }
      end
    end
  end

  # PATCH/PUT /events/1/preview
  def preview
    @event.attributes = event_params
    @event.valid?
    render action: :edit
  end

  def edit; end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update event_params
        format.html { redirect_to @event, notice: t('.ok') }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        status = :unprocessable_entity
        format.json { render json: @event.errors, status: status }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.reason = params[:event][:reason]
    @event.destroy
    respond_to do |format|
      format.html { redirect_to :root, notice: t('.ok') }
      format.json { head :no_content }
    end
  end

  private

  def set_events
    # The 3000 limit is purely arbitrary...
    @events = apply_scopes Event.moderated.limit(3000)
    @events = @events.limit(20) if params[:format] == 'rss'
  end

  def new_event
    time = Time.zone.now.change(min: 0) + 1.day
    params[:event] ||= {
      start_time: time,
      end_time: time + 1.hour,
      region_id: session[:region]
    }
    @event = Event.new event_params
  end

  # Use callbacks to share common setup or constraints between actions
  def set_event
    @event = Event.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through
  def event_params
    params.require(:event)
          .permit :lock_version, :title, :start_time, :end_time, :repeat, :rule,
                  :description, :place_name, :address, :city, :region_id,
                  :locality, :url, :contact, :submitter, :tag_list
  end

  def locked
    redirect_to edit_event_url(@event, secret: @event.secret),
                alert: t('staleObjectError')
  end

  # Check that you can only edit an existing event if you know its secret
  def check_secret
    redirect_to :root, alert: t(:forbidden, scope: %i[events edit]) \
      unless params[:secret] == @event.secret
  end

  # Should the future scope be applied?
  # Only on non html pages...
  def future?
    params[:format].present? && params[:format] != 'html'
  end
end
