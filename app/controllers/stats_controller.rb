# Generate statistics, around events, by date or place
class StatsController < ApplicationController
  has_scope :city, :region, :tag
  has_scope :near, type: :hash, using: %i[location distance]

  before_action :set_events, :counts, :temporal, :local, only: [:index]

  private

  def counts
    @events_count = @events.count :all
    @events_um_count = apply_scopes(Event.unmoderated).count :all
    @orgas_count = apply_scopes(Orga).moderated.count :all
    @orgas_um_count = apply_scopes(Orga.unmoderated).count :all
  end

  def temporal
    @years = @events.group(year_grouping).count :all
    @months = @events.group(year_grouping, month_grouping).count :all
  end

  def local
    @region_events = @events.group(:region_id, year_grouping).count :all
    @regions = Region.all.find_all do |region|
      @years.sum { |year| @region_events[[region.id, year[0]]] || 0 }.positive?
    end
    @city_events = @events.group(:city).having('count(city) > 3')
                          .order('count(city) desc').count :all
  end

  def set_events
    # Remove the ordering which can occur after geocoding
    @events = apply_scopes(Event.unscoped.moderated).reorder nil
  end

  def year_grouping
    if %w[Mysql2 MySQL PostgreSQL].include? Event.connection.adapter_name
      'extract(year from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      'strftime("%Y", start_time)'
    end
  end

  def month_grouping
    if %w[Mysql2 MySQL PostgreSQL].include? Event.connection.adapter_name
      'extract(month from start_time)'
    elsif Event.connection.adapter_name == 'SQLite'
      'strftime("%m", start_time)'
    end
  end
end
