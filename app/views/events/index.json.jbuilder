json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :start_time, :end_time,
                :submission_time, :decision_time,
                :place_name, :address, :city, :region_id, :locality, :url,
                :contact, :tags
  json.url event_url(event, format: :json)
end
