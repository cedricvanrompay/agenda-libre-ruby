require 'differ/format/patch'
Differ.format = Differ::Format::Patch

def gen_title
  t('layouts.application.title') +
    if session[:region].present?
      " [#{Region.find session[:region]}]"
    else
      ''
    end
end

def meta(xml)
  xml.items do
    xml.rdf :Seq do
      @versions.each do |version|
        xml.rdf :li, 'rdf:resource' =>
        polymorphic_url(version.item_type.tableize.singularize,
                        id: version.item_id)
      end
    end
  end
end

def about(xml)
  xml.title gen_title
  xml.description t 'layouts.application.subtitle'
  xml.link root_url
  xml.dc :language, 'fr'
  xml.dc :creator, root_url

  meta xml
end

def version_changes(version)
  version.changeset.collect do |key, val|
    "#{version.item_type.constantize.human_attribute_name(key)}: " +
      if key == 'description'
        Differ.diff(val[1], val[0]).to_s
      else
        "#{val[0]} → #{val[1]}"
      end
  end
end

xml.instruct!

xml.rdf :RDF,
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/' do
  xml.channel 'rdf:about' => root_url do
    about xml
  end

  @versions.reject { |version| version.event == 'create' }.each do |version|
    object = version.item_type.constantize.find_by id: version.item_id
    next unless object.try(:moderated?)

    object = version.reify if object.nil?
    url = polymorphic_url(version.item_type.tableize.singularize,
                          id: version.item_id)

    xml.item 'rdf:about' => url do
      xml.title "#{version.event} - #{object}"
      xml.link url
      xml.dc :identifier, "version_#{version.id}@#{request.domain}"
      xml.dc :date, version.created_at.iso8601

      if object.try(:description)
        changes = version_changes(version).join "\n"
        xml.description changes
        xml.content(:encoded) { xml.cdata! changes }
      end
    end
  end
end
