json.array!(@events) do |e|
  json.merge!(
    type: 'Feature',
    properties: {
      id: e.id,
      name: e.title,
      start_time: e.start_time, end_time: e.end_time,
      submission_time: e.submission_time,
      decision_time: e.decision_time,
      place_name: e.place_name, address: e.address, city: e.city,
      region: t(e.region.code.presence ? e.region.code : e.region.name,
                scope: :countries, default: e.region.name),
      region_id: e.region_id,
      tags: e.tag_list,
      popupContent: link_to(e, event_url(e))
    },
    geometry: {
      type: 'Point',
      coordinates: [e.longitude, e.latitude]
    }
  )
end
