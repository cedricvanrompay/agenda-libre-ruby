# Sending mails related to events' notes
class NoteMailer < ApplicationMailer
  helper :events

  def notify(note)
    @note = note

    host = ActionMailer::Base.default_url_options[:host]
    mail 'In-Reply-To' =>
      "<event-#{note.event.id}@#{host}>",
         to: note.event.submitter,
         subject: subject(note.event),
         'Content-Language': locale
  end

  def create(note)
    @note = note

    mail 'In-Reply-To' =>
      "<mod-#{note.event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: subject(note.event),
         'Content-Language': locale
  end

  private

  def subject(event)
    t('mail_prefix') +
      t("#{mailer_name}.#{action_name}.subject", subject: event.title)
  end
end
