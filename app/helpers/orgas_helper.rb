# Helper for the orga views
module OrgasHelper
  def orga_meta(orga)
    set_meta_tags \
      description: strip_tags(orga.description),
      DC: { title: orga.name },
      geo: {
        placename: orga.city,
        region: orga.region,
        position: "#{orga.latitude};#{orga.longitude}",
        ICBM: "#{orga.latitude}, #{orga.longitude}"
      }
  end
end
