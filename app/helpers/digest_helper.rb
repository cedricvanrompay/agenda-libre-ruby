# Helper for the digest/markdown views
module DigestHelper
  # Important mechanism, to ensure the digest will not have the same footnote
  # link multiple times
  def renumber_footnotes(id, description)
    return if description.nil? || description.blank?

    description.gsub(/\[(\d)+\]/, "[#{id}_\\1]")
  end

  def count_by_country(events)
    events.group_by { |e| e.region&.region || e.region }.collect do |region, es|
      "#{t region.code, scope: :countries, default: region.code}: #{es.size}"
    end.join ', '
  end

  # [country city] title - date
  def to_title(event)
    ["[#{event.region.region&.code || event.region&.code}",
     "#{event.city}]",
     "[#{event.title}](#{event_url event})",
     '-',
     display_date(event)].join ' '
  end
end
