# Helper for the event views
module EventsHelper
  def events_meta
    set_meta_tags \
      description: t('layouts.application.subtitle'),
      keywords: params[:tag],
      DC: {
        title: t('layouts.application.title'),
        subject: t('layouts.application.subtitle'),
        publisher: 'april'
      }
  end

  def event_meta(event)
    set_meta_tags \
      keywords: event.tag_list,
      DC: { title: event.title, date: event.start_time.to_s },
      geo: {
        placename: event.city,
        region: event.region,
        position: "#{event.latitude};#{event.longitude}",
        ICBM: "#{event.latitude}, #{event.longitude}"
      }
  end

  def display_date(event)
    if event.start_time.to_date == event.end_time.to_date
      display_sameday event
    else
      display_multi_days event
    end
  end

  def display_sameday(event)
    t 'date.formats.same_day',
      date: l(event.start_time.to_date, format: :at),
      start: l(event.start_time, format: :hours),
      end: l(event.end_time, format: :hours)
  end

  def display_multi_days(event)
    t 'date.formats.period',
      start: l(event.start_time, format: :at),
      end: l(event.end_time, format: :at)
  end

  # Select the events to display in a month, sorted
  def month_events(events, date)
    events.select { |e| (e.start_time.to_date..e.end_time.to_date).cover? date }
          .sort_by(&:city)
  end

  def display_attr(item, label, value = item[label])
    # return unless value
    item.class.human_attribute_name(label).rjust(12) + " #{value}"
  end

  # Using kramdown, let's parse the html and render it as markdown text
  # No idea why, but also needs to remove extraneous quote encoding :(
  def to_markdown(desc, line_width = -1)
    return '' if desc.blank?

    Kramdown::Document.new(spaces(desc), input: :html, line_width: line_width)
                      .to_kramdown
                      .gsub(/^#+\s+(.*)/, '**\1**')
                      .gsub(/\*\*\*\*/, '**')
                      .gsub(/\\(["'])/, '\1') # Remove slash before quotes
                      .remove(/[[:blank:]]+$/) # Remove extraneous spaces
                      .remove(/{::}/) # Markdown artefact
  end

  private

  # Cleaner html elements, to correct things like <em> test</em>
  def spaces(desc)
    desc = sanitize desc,
                    tags: %w[p br h1 h2 h3 h4 table tr th td ul ol li a strong
                             b em i sub sup],
                    attributes: %w[href]

    desc.gsub(/<(strong|em|b|i)> /, ' <\1>')
        .gsub(%r{ </(strong|em|b|i)>}, '</\1> ')
        .gsub(/[[:space:]]([,.])/, '\1')
  end
end
