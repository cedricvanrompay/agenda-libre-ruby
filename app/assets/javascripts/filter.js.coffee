# Cleans up filter submission, for cleaner URL
$(document).on 'turbolinks:load', ->
	$('body.pages form :input').prop 'disabled', false

	$('form').submit ->
		$('input[name=utf8]').prop 'disabled', true
		$('button').prop 'disabled', true
