$(document).on 'turbolinks:load', ->
	return unless navigator.geolocation

	$('a.near-me').click (event) ->
		event.preventDefault()

		# We store the url we're going to
		window.goto = event.target.href

		navigator.geolocation.getCurrentPosition showPosition, (error) ->
			switch error.code
				when error.PERMISSION_DENIED
					$('ul.regions li#near-me').remove()

	$('body.pages.show form').submit ->
		# Juggle between near location and city
		if $('#near_location').val() && $('#near_distance').val()
			$('#city').val('')

		else
			$('#city').val($('#near_location').val())
			$('#near_location').val('')

showPosition = (position) ->
	location.replace window.goto.replace '[me]',
		"[#{position.coords.latitude.toFixed(2)}, #{position.coords.longitude.toFixed(2)}]"
