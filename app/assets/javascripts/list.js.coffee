$(document).on 'turbolinks:load', ->
	$('table.list td.view a').each -> visit $(this)

  # Automatic ajax pagination
	$('.pagination .next a').attr('data-remote', true).each ->
		# Go to the next page when page is scrolled
		$(document).scroll =>
			if $(this).visible true, true
				# "Next" link is also hidden while pagination is done
				$(this).click().parents('.pagination').hide()

$(document).on 'ajax:success', '.pagination .next a', (event, data) ->
	$(this).parents('tfoot')
		.prev()
		.append($('tbody tr', data))
		.find('td.view a').each ->
      visit $(this)

	next = $('.pagination .next a', data).attr 'href'
	if next?
		$(this).attr('href', next).parents('.pagination').show()

# Clicking on a table.list row
visit = (elt) =>
	elt.closest('tr').addClass('view').click (event) ->
		target = $(event.target)
		unless target.attr('target')? or target.parents('a').attr('target')?
			Turbolinks.visit elt.attr 'href'

