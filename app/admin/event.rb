ActiveAdmin.register Event do
  permit_params :title, :start_time, :end_time, :repeat, :rule, :description,
                :place_name, :address, :city, :region_id, :locality, :url,
                :contact, :submitter, :tag_list

  scope :all
  scope :moderated, default: true
  scope :unmoderated

  config.sort_order = 'start_time_desc'

  filter :region, multiple: true
  filter :start_time
  filter :base_tags

  index do
    column :title
    column :start_time
    column :end_time
    column :city
    column :region
    column :repeat
    column :rule
    column :moderated
    actions
  end

  csv do
    column :title
    column :start_time
    column :end_time
    column :place_name
    column :address
    column :city
    column :region
    column :locality
    column :url
    column :contact
    column :submitter
    column :tag_list
    column :repeat
    column :rule
  end

  show do
    attributes_table do
      row :title
      row :start_time
      row :end_time
      row :place_name
      row :address
      row :city
      row :region
      row :description do
        simple_format event.description
      end
      row :locality
      row :url do
        link_to event.url, event.url
      end
      row :contact do
        mail_to event.contact, event.contact
      end
      row :submitter do
        mail_to event.submitter, event.submitter
      end
    end
    active_admin_comments
  end
end
