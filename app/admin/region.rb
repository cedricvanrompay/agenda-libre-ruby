ActiveAdmin.register Region do
  permit_params :region_id, :code, :name, :url
end
