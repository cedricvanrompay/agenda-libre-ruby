ActiveAdmin.register I18n::Backend::ActiveRecord::Translation do
  permit_params :locale, :key, :value

  index do
    column :locale
    column :key
    actions
  end

  show do |t|
    h3 "#{t.locale} - #{t.key}"
    section do
      simple_format t.value
    end
  end

  form do |f|
    f.inputs do
      f.input :locale
      f.input :key
      f.input :value
      f.actions
    end
  end

  after_update do
    I18n.reload!
  end
end
