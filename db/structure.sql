CREATE TABLE "schema_migrations" ("version" varchar NOT NULL);
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
CREATE TABLE "active_admin_comments" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "namespace" varchar(255), "body" text(65535), "resource_id" varchar(255) NOT NULL, "resource_type" varchar(255) NOT NULL, "author_id" integer(4), "author_type" varchar(255), "created_at" datetime, "updated_at" datetime);
CREATE INDEX "index_active_admin_comments_on_author_type_and_author_id" ON "active_admin_comments" ("author_type", "author_id");
CREATE INDEX "index_active_admin_comments_on_namespace" ON "active_admin_comments" ("namespace");
CREATE INDEX "index_active_admin_comments_on_resource_type_and_resource_id" ON "active_admin_comments" ("resource_type", "resource_id");
CREATE TABLE "admin_users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar(255) DEFAULT '' NOT NULL, "encrypted_password" varchar(255) DEFAULT '' NOT NULL, "reset_password_token" varchar(255), "reset_password_sent_at" datetime, "remember_created_at" datetime, "sign_in_count" integer(4) DEFAULT 0 NOT NULL, "current_sign_in_at" datetime, "last_sign_in_at" datetime, "current_sign_in_ip" varchar(255), "last_sign_in_ip" varchar(255), "created_at" datetime, "updated_at" datetime);
CREATE UNIQUE INDEX "index_admin_users_on_email" ON "admin_users" ("email");
CREATE UNIQUE INDEX "index_admin_users_on_reset_password_token" ON "admin_users" ("reset_password_token");
CREATE TABLE "cities" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) DEFAULT '' NOT NULL, "majname" varchar(255) DEFAULT '' NOT NULL, "postalcode" integer(4), "inseecode" integer(4), "regioncode" integer(4), "latitude" float(24), "longitude" float(24));
CREATE INDEX "cities_name" ON "cities" ("name");
CREATE TABLE "events" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar(255) DEFAULT '' NOT NULL, "description" text(65535) NOT NULL, "start_time" datetime NOT NULL, "end_time" datetime NOT NULL, "city" varchar(255) DEFAULT '', "region_id" integer(4) DEFAULT 0 NOT NULL, "locality" integer(4) DEFAULT 0 NOT NULL, "url" varchar(255) DEFAULT '' NOT NULL, "contact" varchar(255) DEFAULT '', "submitter" varchar(255) DEFAULT '' NOT NULL, "moderated" integer(4) DEFAULT 0 NOT NULL, "tags" varchar(255) DEFAULT '' NOT NULL, "secret" varchar(255) DEFAULT '' NOT NULL, "decision_time" datetime, "submission_time" datetime, "moderator_mail_id" varchar(32), "submitter_mail_id" varchar(32), "address" text(65535), "latitude" float(24), "longitude" float(24), "lock_version" integer(4) DEFAULT 0 NOT NULL, "place_name" varchar(255), "count" integer DEFAULT 1, "repeat" integer DEFAULT 0, "rule" text, "event_id" integer);
CREATE INDEX "index_events_on_event_id" ON "events" ("event_id");
CREATE INDEX "events_date" ON "events" ("start_time", "end_time");
CREATE TABLE "kinds" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "icon" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "notes" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "contents" text(65535) NOT NULL, "date" datetime NOT NULL, "event_id" integer(4), "author_id" integer(4));
CREATE TABLE "orgas" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "region_id" integer(4) DEFAULT 0 NOT NULL, "department" varchar(4) DEFAULT '0' NOT NULL, "name" varchar(255) DEFAULT '' NOT NULL, "url" varchar(255) DEFAULT '' NOT NULL, "city" varchar(255) DEFAULT '', "kind_id" integer(4), "feed" varchar(255), "contact" varchar(255), "submitter" varchar(255), "moderated" boolean(1) DEFAULT 'f', "submission_time" datetime, "decision_time" datetime, "secret" varchar(255), "deleted" boolean(1) DEFAULT 'f' NOT NULL, "active" boolean DEFAULT 't' NOT NULL, "description" text, "created_at" datetime, "updated_at" datetime, "tag" text, "tags" text DEFAULT '', "diaspora" text, "object_changes" text, "place_name" text, "address" text, "latitude" float, "longitude" float);
CREATE INDEX "index_orgas_on_kind_id" ON "orgas" ("kind_id");
CREATE TABLE "regions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) DEFAULT '' NOT NULL);
CREATE TABLE "taggings" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "tag_id" integer, "taggable_id" integer, "taggable_type" varchar(255), "tagger_id" integer, "tagger_type" varchar(255), "context" varchar(128), "created_at" datetime);
CREATE UNIQUE INDEX "taggings_idx" ON "taggings" ("tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type");
CREATE INDEX "index_taggings_on_taggable_id_and_taggable_type_and_context" ON "taggings" ("taggable_id", "taggable_type", "context");
CREATE TABLE "tags" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "taggings_count" integer DEFAULT 0);
CREATE UNIQUE INDEX "index_tags_on_name" ON "tags" ("name");
CREATE TABLE "translations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "locale" varchar(255), "key" varchar(255), "value" text(65535), "interpolations" text(65535), "is_proc" boolean(1) DEFAULT 'f');
CREATE TABLE "users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "login" varchar(255) DEFAULT '' NOT NULL, "password" varchar(255) DEFAULT '' NOT NULL, "email" varchar(255) DEFAULT '' NOT NULL, "lastname" varchar(255) DEFAULT '' NOT NULL, "firstname" varchar(255) DEFAULT '' NOT NULL, "confirmation_token" varchar, "confirmed_at" datetime, "confirmation_sent_at" datetime, "remember_created_at" datetime);
CREATE UNIQUE INDEX "index_users_on_confirmation_token" ON "users" ("confirmation_token");
CREATE TABLE "versions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_type" varchar NOT NULL, "item_id" integer NOT NULL, "event" varchar NOT NULL, "whodunnit" varchar, "object" text, "created_at" datetime, "object_changes" text(1073741823));
CREATE INDEX "index_versions_on_item_type_and_item_id" ON "versions" ("item_type", "item_id");
INSERT INTO schema_migrations (version) VALUES ('20131114103118');

INSERT INTO schema_migrations (version) VALUES ('20131114103121');

INSERT INTO schema_migrations (version) VALUES ('20140403204748');

INSERT INTO schema_migrations (version) VALUES ('20140823111115');

INSERT INTO schema_migrations (version) VALUES ('20141011100700');

INSERT INTO schema_migrations (version) VALUES ('20150215121248');

INSERT INTO schema_migrations (version) VALUES ('20150215151059');

INSERT INTO schema_migrations (version) VALUES ('20150215172000');

INSERT INTO schema_migrations (version) VALUES ('20150215172739');

INSERT INTO schema_migrations (version) VALUES ('20150301223829');

INSERT INTO schema_migrations (version) VALUES ('20150711164423');

INSERT INTO schema_migrations (version) VALUES ('20151225194836');

INSERT INTO schema_migrations (version) VALUES ('20151226223947');

INSERT INTO schema_migrations (version) VALUES ('20160102152958');

INSERT INTO schema_migrations (version) VALUES ('20160103141244');

INSERT INTO schema_migrations (version) VALUES ('20160107203117');

INSERT INTO schema_migrations (version) VALUES ('20160109203136');

INSERT INTO schema_migrations (version) VALUES ('20160111124855');

INSERT INTO schema_migrations (version) VALUES ('20160409131029');

INSERT INTO schema_migrations (version) VALUES ('20160616190823');

INSERT INTO schema_migrations (version) VALUES ('20161031215120');

