# Manage a schedule for events, that will let adl create recurring events
class CreateSchedules < ActiveRecord::Migration
  def change
    add_column :events, :repeat, :integer, default: 0
    add_column :events, :rule, :text
    # This column is there to manage scheduled events
    add_reference :events, :event, index: true
  end
end
