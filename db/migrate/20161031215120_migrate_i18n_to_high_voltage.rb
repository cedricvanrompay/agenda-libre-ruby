# Migrate some i18n translations in the database to the high_voltage static
# page CMS
class MigrateI18nToHighVoltage < ActiveRecord::Migration
  def up
    update "
      update translations
      set `key` = replace(`key`, 'application', 'pages')
      where `key` like 'application.%'
    "
  end

  def down
    update "
      update translations
      set `key` = replace(`key`, 'pages', 'application')
      where `key` like 'pages.%'
    "
  end
end
