# Events and orgas do not require a city anymore
class ModifyCityNullable < ActiveRecord::Migration
  def change
    change_column :events, :city, :string, default: '', null: true
    change_column :orgas, :city, :string, default: '', null: true
  end
end
