# Add optimistic locking to events, so that moderators won't risk overwriting
# each others' work
class AddLockVersionToEvents < ActiveRecord::Migration
  def change
    add_column :events, :lock_version, :integer, default: 0, null: false
  end
end
